// List of animals
const animals = ['dogs', 'cats', 'lions', 'tigers', 'rats', 'snakes', 'rabbits', 'zebras', 'wolvies', 'turkeys', 'chickens', 'pigeons', 'boars', 'birds', 'kangaroos', 'gorillas', 'monkeys', 'tortoise', 'sheeps', 'antelopes', 'peacocks', 'lizards', 'jaguars', 'leapards', 'hippopotamus', 'giraffes', 'frogs', 'fox', 'elephants', 'ducks', 'dragons', 'cows', 'pigs', 'coyotes', 'cougars', 'bats'];

// selecting the input tag at the html 
const userInput = document.querySelector("input");

// selecting the class btn at the html 
const btn = document.getElementsByClassName('btn');

// adding an event listener for when a key is pressed
userInput.addEventListener('keypress', updateUserInput);

// the function that is activated when the enter key on the keyboard is pressed
function updateUserInput(e) {
	// setting keypress to a particular keycode
	if(e.keyCode === 13) {

		// changing what the user inputs to lowercase
		let filter = e.target.value.toLowerCase();

		// checks if filter is equal to the animal array
		if (animals.includes(filter)) {
			alert('Yipee!!! we have ' + filter + ' in our array.');
 		} else {
 			  alert('Dammit it!!! Better luck next time buddy.');
 			}

 		// clears the input tag when keycode 13 is pressed
 		userInput.value = "";
	}	
}

